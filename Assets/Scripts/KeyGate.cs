
using UnityEngine;

public class KeyGate : MonoBehaviour
{
    [SerializeField] private Gate _gate;
    

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent(out Player player))
        {
            Destroy(_gate.gameObject);
            Destroy(gameObject);
        }
    }
}
